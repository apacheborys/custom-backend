<?php

namespace MyFirstView\Providers;

use Plenty\Plugin\RouteServiceProvider;
use Plenty\Plugin\Routing\ApiRouter;

/**
 * Class MyFirstRouteServiceProvider
 */

class MyFirstViewRouteServiceProvider extends RouteServiceProvider
{
    /**
     * @param ApiRouter $api
     */
    public function map(ApiRouter $api)
    {
        $api->version(['v1'], ['middleware' => ['oauth']], function ($router) {
            $router->get('markets/custom/settings/all', [
                'uses'       => 'MyFirstView\Controllers\SettingsController@all'
            ]);

            $router->get('markets/custom/settings/shops', [
                'uses'       => 'MyFirstView\Controllers\SettingsController@getShops'
            ]);

        });
    }
}