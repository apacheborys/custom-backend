<?php

namespace MyFirstView\Controllers;

use Plenty\Plugin\Controller;
use Plenty\Plugin\Http\Request;
use Plenty\Plugin\Http\Response;

/**
 * Class SettingsController
 */
class SettingsController extends Controller
{
    /**
     * Get all settings.
     *
     * @return array
     * @throws \Exception
     */
    public function all()
    {
        return [
            'shop' => [
                'shopId' => null,
                'mainLanguage' => 'de',
                'exportLanguages' => [],
                'processes' => []
            ],
        ];
    }

    /**
     * Get all Custom shops.
     *
     * @return array
     * @throws \Exception
     */
    public function getShops()
    {
        return [];
    }

}